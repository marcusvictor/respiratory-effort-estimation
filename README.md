- Before using the respiratory estimation methods, you need to set up both 
YALMIP and Gurobi on your machine.
- To setup YALMIP, first download YALMIP from https://yalmip.github.io/ and 
place its contents (YALMIP-master folder) at the root of the repository.
- Gurobi is a commercial state-of-the-art solver. If you are in academia, you are
eligible to obtain a free license of Gurobi. Please visit https://www.gurobi.com/
for instructions on how to download and validate Gurobi.
- The respiratory dynamics simulator was developed in Python and its source code 
is within folder `waveforms_generator`. The main script is `waveforms_generator.py`. 
To change the simulator parameters, please see `parameters_set.py`.
- The respiratory effort estimation itself was developed in MATLAB in order to 
use YALMIP for faster optimization prototyping. The main MATLAB functions are:
    - `pmus_miqp.m`: executes the respiratory estimation using our method based on
mixed-integer quadratic programming.
    - `pmus_qp.m`: executes the respiratory estimation using the method by Vicario 
et al., which is based on quadratic programming.
    - `plot_pmus.m`: plots the respiratory estimation waveforms, as shown in figures
throughout the paper.
    - `set_yalmip`: setup YALMIP. Assumes that YALMIP is within a folder named `YALMIP-master`
    at the root of the repository.
    - `set_gurobi`: setup Gurobi. This script was made for setting up Gurobi 8.1.1
on Windows. Please, adapt the script to your own configuration.
- Within estimation, there are folders with scripts and data to reproduce the 
results shown in the paper:
    - `qp_miqp_comparison`: reproduces results from Subsection 3.1.
    - `start_of_inspiration`: reproduces results from Subsection 3.2.
    - `other_pmus`: reproduces results from Subsection 3.3.
    - `epsilon_discussion`: reproduces results from Subsection 4.5.
