function plot_pmus(fig_title, waveforms_true, waveforms_hat, fs, switching_times)
% PLOT_PMUS Plots pmus estimation waveforms.
% fig_title: title used for figures.
% waveforms_true: ground truth waveforms.
% waveforms_hat: estimated waveforms.
% fs: sampling frequency.
% switching_times: time instants where pmus behavior change.
N = size(waveforms_true.pmus, 1);

if nargin < 5
    Ns = 0;
else
    Ns = length(switching_times);
end

pmus_true = waveforms_true.pmus;
pmus_hat = waveforms_hat.pmus;
pao_true = waveforms_true.pao;
pao_hat = waveforms_hat.pao;

font_size = 14;
line_width = 2;

blue = [0, 0.4470, 0.7410];
red = [0.8500, 0.3250, 0.0980];
yellow = [0.9290, 0.6940, 0.1250];
purple = [0.4940, 0.1840, 0.5560];
green = [0.4660, 0.6740, 0.1880];
light_blue = [0.3010, 0.7450, 0.9330];

st_colors = {yellow, purple, green};

figure
hold on
plot((0:N-1) / fs, pmus_true, 'color', blue, 'LineWidth', line_width)
plot((0:N-1) / fs, pmus_hat, '--', 'color', red, 'LineWidth', line_width)
for i=1:Ns
    k = (switching_times(i) - 1) / fs;
    plot(k, [0], 'o', 'color', st_colors{i}, 'LineWidth', line_width);
end
for i=1:Ns
    k = (switching_times(i) - 1) / fs;
    plot([k, k], [0, pmus_hat(int32(switching_times(i)))], '--', 'color', st_colors{i});
end
set(gca, 'FontSize', font_size);
title(fig_title, 'FontSize', font_size, 'interpreter', 'latex');
% title('Test case (c): $P_{mus}(t)$ parexp, $t_p=0.45$, $t_r=0.6$', 'FontSize', 14, 'interpreter', 'latex');
xlabel('Time ($s$)', 'FontSize', font_size, 'interpreter', 'latex');
ylabel('Pressure ($cm H_2O$)', 'FontSize', font_size, 'interpreter', 'latex');
legs = {'$\tilde{P}_{mus}(t)$ true', '$\tilde{P}_{mus}(t)$ estimated'};
for i=1:Ns
    legs{end + 1} = sprintf('$t_{s%d}$', i);
end
legend(legs, 'FontSize', font_size, 'interpreter', 'latex', 'location', 'southeast')
grid

figure
plot((0:N-1) / 50, pao_true, 'color', blue, 'LineWidth', line_width)
hold on
plot((0:N-1) / 50, pao_hat, '--', 'color', red, 'LineWidth', line_width)
legend('$P_{ao}(t)$ true','$P_{ao}(t)$ estimated', 'FontSize', font_size, 'interpreter', 'latex')
xlabel('Time ($s$)', 'FontSize', font_size, 'interpreter', 'latex');
ylabel('Pressure ($cm H_2O$)', 'FontSize', font_size, 'interpreter', 'latex');
set(gca, 'FontSize', font_size);
grid

end