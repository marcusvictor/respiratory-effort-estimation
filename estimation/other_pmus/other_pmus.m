addpath('..')
table = zeros(4, 3);
[waveforms_true, waveforms_hat, params_true, params_hat, solver_time, switching_times] = pmus_miqp('case_a.bin', true);
plot_pmus('Test case (a): $P_{mus}(t)$ linear, $t_p = 0.45 \ s$, $t_r = 0.6 \ s$', waveforms_true, waveforms_hat, fs, switching_times);
table(1, 1) = params_hat.resistance;
table(1, 2) = params_hat.elastance;
table(1, 3) = (switching_times(1) - 1) / fs;
table(1, 4) = (switching_times(2) - 1) / fs;
table(1, 5) = (switching_times(3) - 1) / fs;
table(1, 6) = solver_time;
[waveforms_true, waveforms_hat, params_true, params_hat, solver_time, switching_times] = pmus_miqp('case_b.bin', true);
plot_pmus('Test case (b): $P_{mus}(t)$ linear, $t_p = 0.6 \ s$, $t_r = 1.2 \ s$', waveforms_true, waveforms_hat, fs, switching_times);
table(2, 1) = params_hat.resistance;
table(2, 2) = params_hat.elastance;
table(2, 3) = (switching_times(1) - 1) / fs;
table(2, 4) = (switching_times(2) - 1) / fs;
table(2, 5) = (switching_times(3) - 1) / fs;
table(2, 6) = solver_time;
[waveforms_true, waveforms_hat, params_true, params_hat, solver_time, switching_times] = pmus_miqp('case_c.bin', true);
plot_pmus('Test case (c): $P_{mus}(t)$ parexp, $t_p = 0.45 \ s$, $t_r = 0.6 \ s$', waveforms_true, waveforms_hat, fs, switching_times);
table(3, 1) = params_hat.resistance;
table(3, 2) = params_hat.elastance;
table(3, 3) = (switching_times(1) - 1) / fs;
table(3, 4) = (switching_times(2) - 1) / fs;
table(3, 5) = (switching_times(3) - 1) / fs;
table(3, 6) = solver_time;
[waveforms_true, waveforms_hat, params_true, params_hat, solver_time, switching_times] = pmus_miqp('case_d.bin', true);
plot_pmus('Test case (d): $P_{mus}(t)$ parexp, $t_p = 0.6 \ s$, $t_r = 1.2 \ s$', waveforms_true, waveforms_hat, fs, switching_times);
table(4, 1) = params_hat.resistance;
table(4, 2) = params_hat.elastance;
table(4, 3) = (switching_times(1) - 1) / fs;
table(4, 4) = (switching_times(2) - 1) / fs;
table(4, 5) = (switching_times(3) - 1) / fs;
table(4, 6) = solver_time;

table