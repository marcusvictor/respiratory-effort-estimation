addpath('..')
fs = 50;
filename = 'comparison055.bin';
[waveforms_true, waveforms_hat, params_true, params_hat, solver_time, switching_times] = pmus_miqp(filename);
plot_pmus('$E_{cycle}=0.55$ -- MIQP', waveforms_true, waveforms_hat, fs, switching_times);
[waveforms_true, waveforms_hat, params_true, params_hat, solver_time, switching_times] = pmus_qp(filename);
plot_pmus('$E_{cycle}=0.55$ -- QP', waveforms_true, waveforms_hat, fs, switching_times);
