fs = 50;
ecycles = [0.15, 0.20, 0.25, 0.30, 0.35, 0.40, 0.45, 0.50, 0.55];

num_cases = length(ecycles);

filenames = {'comparison015.bin',...
     'comparison020.bin',...
    'comparison025.bin',...
    'comparison030.bin',...
    'comparison035.bin',...
    'comparison040.bin',...
    'comparison045.bin',...
    'comparison050.bin',...
    'comparison055.bin'};

addpath('..');

num_runs = 1;

solution_table = zeros(num_cases, 8);

for i=1:num_cases
    solution_table(i, 1) = ecycles(i);
end

time_table = zeros(num_cases, 3);
for i=1:num_cases
    time_table(i, 1) = ecycles(i);
end

for i=1:length(filenames)
    filename = filenames{i};
    average_time = 0;
    for j=1:num_runs
        [waveforms_true, waveforms_hat, params_true, params_hat, solver_time, switching_times] = pmus_qp(filename);
        average_time = average_time + solver_time;
    end
    solution_table(i, 2) = (switching_times(2) - 1) / fs;
    solution_table(i, 3) = 0.6;
    solution_table(i, 5) = params_hat.resistance;
    solution_table(i, 6) = params_hat.elastance;
    time_table(i, 2) = average_time / num_runs;
    average_time = 0;
    for j=1:num_runs
        [waveforms_true, waveforms_hat, params_true, params_hat, solver_time, switching_times] = pmus_miqp(filename);
        average_time = average_time + solver_time;
    end
    solution_table(i, 4) = (switching_times(2) - 1) / fs;
    solution_table(i, 7) = params_hat.resistance;
    solution_table(i, 8) = params_hat.elastance;
    time_table(i, 3) = average_time / num_runs;
end

solution_table

time_table