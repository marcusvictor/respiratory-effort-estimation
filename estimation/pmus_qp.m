function [waveforms_true, waveforms_hat, params_true, params_hat,...
    solver_time, switching_times] = pmus_qp(filename)
% PMUS_QP Estimates the Pmus waveform and the respiratory dynamics' 
% parameters (resistance and elastance) by solving many quadratic 
% programs (method by Vicario et al).
% Input:
%   filename - name of the binary file from the simulator.
% Output:
%   waveforms_true - ground truth values of the waveforms.
%   waveforms_hat - estimated values of the waveforms.
%   params_true - ground truth values of the parameters.
%   params_hat - estimated values of the parameters.
%   solver_time - time to solve the optimization problem.
%   switching_times - switching points of regions.

%% Uncomment if you want to load Gurobi and YALMIP every time (makes the script slower)
% set_gurobi
% set_yalmip

%% Loading waveforms
% filename = 'samplefile015.bin';
[flow, volume, pao, pmus_true, insex] =  load_python_bin(filename);

% k_soe stands for the index where the start of exhalation occurs
k_soe = find(diff(insex) <= -0.5);
k_soe = k_soe(1) + 1; % since we lose 1 index by using diff

% Converting units of airflow
flow = flow / 60 * 1000;

N = length(pmus_true); %number of samples

pmus = sdpvar(N, 1);

best_cost = inf;
total_time = 0;
for t1=1:k_soe-1
    constraint_regions = [];
    for i=1:t1-1
        constraint_regions = constraint_regions + [pmus(i+1) <= pmus(i)];
    end
    for i=t1:k_soe-1
        constraint_regions = constraint_regions + [pmus(i) <= pmus(i+1)];
    end
    for i=k_soe:N-1
        constraint_regions = constraint_regions + [pmus(i+1) == pmus(i)];
    end
    
    resistance = sdpvar(1, 1);
    elastance = sdpvar(1, 1);
    
    cost = (pao - (pmus + resistance * flow + volume * elastance))' * ...
        (pao - (pmus + resistance * flow + volume * elastance));
    
    constraint_real = [ones(N,1)*(-20) <= pmus <= ones(N,1)*1, ...
        0 <= resistance, resistance <= 0.1, 0.005 <= elastance, elastance <= 1];
    
    options = sdpsettings;
    
    solution = optimize([constraint_regions, constraint_real],...
        cost, options);
    
    cost_value = value(cost);
    
    if cost_value < best_cost
        best_cost = cost_value;
        t1_best = t1;
        total_time = total_time + solution.solvertime;
        pmus_best = value(pmus);
        resistance_best = value(resistance);
        elastance_best = value(elastance);
    end
end

solver_time = total_time;

waveforms_true.flow = flow;
waveforms_true.volume = volume;
waveforms_true.pao = pao;
waveforms_true.pmus = pmus_true;
waveforms_true.insex = insex;

waveforms_hat.pmus = pmus_best;
waveforms_hat.pao = waveforms_hat.pmus + resistance_best * flow + volume * elastance_best;

params_lse = (([flow volume]' * [flow volume]) \ ([flow volume]')) * (pao - pmus_true);
params_true.resistance = params_lse(1) * 1000;
params_true.elastance = params_lse(2) * 1000;

params_hat.resistance = resistance_best * 1000;
params_hat.elastance = elastance_best * 1000;

switching_times = zeros(2, 1);
switching_times(1) = t1_best;
switching_times(2) = k_soe;

end