fs = 50;
filename = 'comparison055.bin';
addpath('..')
[waveforms_true, waveforms_hat, params_true, params_hat, solver_time, switching_times] = pmus_miqp(filename, false, false, 20, 0);
plot_pmus('$E_{cycle}=0.55$, $\varepsilon = 0$ -- MIQP', waveforms_true, waveforms_hat, fs, switching_times);