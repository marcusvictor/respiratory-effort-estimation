import parameter_set as params
from solve_model import solve_model
import os
import numpy as np
import time
import matplotlib.pyplot as pt
from array import array


def create_test_cases(tests_list, test_case, params_names, param_id):
    """
    Create the test cases list
    """
    if param_id < len(params_names):
        param_name = params_names[param_id]  # each respiratory cycle parameter
        param_values = getattr(params, param_name)  # take all possibilities for each parameter
        for value in param_values:
            test_case[param_id] = value  # assign that value for test_case variable
            create_test_cases(tests_list, test_case, params_names, param_id + 1)  # do for the next parameter
    else:
        tests_list.append(test_case.copy())  # copy a whole set of parameter to the main list 'tests.list'


# Script initialization
print('---------- Initializing ----------')

# Prints debug messages
debugmsg = False

# Saves waveforms in a file
store_dir = '..\\estimation'
save_file = True

# Parameters for defining each respiratory cycle
params_names = ['Fs', 'Rvent', 'C', 'Rins', 'Rexp', 'PEEP', 'SP',
                'Triggertype', 'Triggerarg', 'Risetype', 'Risetime', 'Cycleoff', 'RR',
                'Pmustype', 'Pp', 'Tp', 'Tf', 'Noise_var', 'E2', 'Model']

tests_list = list()  # Stores all respiratory cycle possibilities
test_case = [None] * len(params_names)  # Stores the parameters of a respiratory cycle

# Create the complete set of respiratory parameters
t = time.time()
create_test_cases(tests_list, test_case, params_names, 0)
print(f'Elapsed time for generating test cases: {time.time() - t}')
num_test_cases = len(tests_list)
print(f'Number of respiratory cycles to be simulated: {num_test_cases}')

Fs = params.Fs
RR = params.RR

fs = Fs[0]
rr = RR[0]

print(f'Creating waveforms for fs={fs} / rr={rr}')

num_points = int(np.floor(60.0 / rr * fs) + 1)

# Target waveforms
flow = np.zeros((num_points, num_test_cases))
volume = np.zeros((num_points, num_test_cases))
paw = np.zeros((num_points, num_test_cases))
pmus = np.zeros((num_points, num_test_cases))
ins = np.zeros((num_points, num_test_cases))

t = time.time()
for i in range(num_test_cases):
    (flow[:, i], volume[:, i], paw[:, i], pmus[:, i], ins[:, i], success) = solve_model(tests_list[i], debugmsg)

print(f'Elapsed time for solving test cases: {time.time() - t}')

# Save the waveforms in a file
if save_file:
    print(f'Creating file for fs={fs} / rr={rr}')
    name = f'fs{fs}_rr{rr}'
    name = 'samplefile'

    if not os.path.isdir(store_dir):
        os.makedirs(store_dir)
        print(f'<./{store_dir}/> folder created.')

    t = time.time()
    output_file = open(f'{store_dir}/{name}.bin', 'wb')
    float_array = np.concatenate((flow.ravel(1), volume.ravel(1), paw.ravel(1), pmus.ravel(1), ins.ravel(1)))
    float_array.tofile(output_file)
    output_file.close()
    print(f'\nElapsed time for writing file: {time.time() - t}')

pt.rc('text', usetex=True)
pt.rc('font', family='serif')
pt.rcParams.update({'font.size': 10})

# Plot the outcomes
# Actual time traces
pt.figure(0)
pt.subplot(311)
pt.plot(np.arange(60.0 / rr * fs + 1) / fs, paw.ravel(1), label=r'$P_{ao}$')
pt.plot(np.arange(60.0 / rr * fs + 1) / fs, pmus.ravel(1), label=r'$P_{mus}$')
pt.ylabel(r'Pressure (cmH2O)')
pt.grid()
pt.legend(loc='upper right')
pt.title(r'Waveforms')

pt.subplot(312)
pt.plot(np.arange(60.0 / rr * fs + 1) / fs, flow.ravel(1), label=r'Flow')
pt.ylabel(r'Flow ($L/min$)')
pt.grid()

pt.subplot(313)
pt.plot(np.arange(60.0 / rr * fs + 1) / fs, volume.ravel(1), label=r'Volume')
pt.ylabel(r'Volume ($mL$)')
pt.xlabel(r'Time ($s$)')
pt.grid()
pt.show()
pt.savefig('fig01.eps', format='eps')

# Waveforms for synchronization assessment
pt.figure(1)
pt.plot(np.arange(60.0 / rr * fs + 1) / fs, paw.ravel(1) / np.max(paw.ravel(1)), label='Paw')
pt.plot(np.arange(60.0 / rr * fs + 1) / fs, pmus.ravel(1) / np.max(np.abs(pmus.ravel(1))), label='Pmus')
pt.plot(np.arange(60.0 / rr * fs + 1) / fs, flow.ravel(1) / np.max(flow.ravel(1)), label='Flow')
pt.plot(np.arange(60.0 / rr * fs + 1) / fs, volume.ravel(1) / np.max(volume.ravel(1)), label='Volume')
pt.plot(np.arange(60.0 / rr * fs + 1) / fs, ins.ravel(1), label='InsEx')
pt.title('Waveforms sync')
pt.xlabel('Time (s)')
pt.ylabel('Normalized values')
pt.legend()
pt.grid()
pt.show()

# PV alveolar curve
pt.figure(2)
for i in range(len(tests_list)):
    pt.plot(volume[:, i], (paw[:, i] - pmus[:, i] - flow[:, i] * 12.0 / 1000 * 1000 / 60))
pt.title('PV-curve')
pt.ylabel('Alveolar pressure (cmH2O)')
pt.xlabel('Volume (mL)')
pt.grid()
pt.show()