import waveform_generation_py.pmus_generation as pmus_gen
import matplotlib.pyplot as pt


fs = 100
rr = 15
pp = -10
tp = 0.4
tf = 0.8

pt.close(fig='all')
pmus = pmus_gen.pmus_profile(fs, rr, 'linear', pp, tp, tf)
pt.plot(pmus)
pt.show()
pmus = pmus_gen.pmus_profile(fs, rr, 'ingmar', pp, tp, tf)
pt.plot(pmus)
pt.show()
pmus = pmus_gen.pmus_profile(fs, rr, 'parexp', pp, tp, tf)
pt.plot(pmus)
pt.show()