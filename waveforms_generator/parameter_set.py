Fs = [50]           #sample frequency, Hertz (Hz)
Noise_var = [0] #noise variance over pressure/flow waveforms

Rvent = [0]          #ventilator expiratory valve resistance, cmH2O/(L/s)
Model = ['FOLM'] #respiratory system model
C = [30]    #respiratory system compliance, (mL/cmH2O)
E2 = [0]#-30, -20, -10, 0, 10, 20, 30]
Rins = [5]     #respiratory system inspiratory resistance, cmH2O/(L/s)
Rexp = [5]     #respiratory system  expiratory resistance, cmH2O/(L/s)

PEEP = [0]      #positive end-expiratory pressure, water-centimeters (cmH2O)
SP = [12]         #support pressure (above PEEP) (cmH2O)
Triggertype = ['flow']      #ventilator trigger type
# Triggertype = ['flow', 'pressure', 'delay']      #ventilator trigger type
Triggerflow = [2]#2]                          #airflow, (L/min)
Triggerpressure = [-0.5, -1, -2]                 #pressure (cmH2O)
Triggerdelay = [0.05, 0.10]                     #delay time (s)
Triggeroption = [Triggertype[0]]
Triggerarg = Triggerflow
Cycleoff = [0.25]                     #turns off the support, after flow fall below x% of the peak flow
Risetype = ['exp']                    #pressure waveform rises in exponential or linear fashions
Risetime = [0.3]                    #time (s) to pressure waveform rises from PEEP to SP

RR = [15]   #respiratory rate, respirations per minute (rpm)
Pmustype = ['parexp']        #morphology of the respiratory effort
Pp = [-8]#, -10, -12]    #Pmus negative peak amplitude (cmH2O)
Tp = [0.3]#, 0.5]   #Pmus negative peak time (s)
Tf = [0.6]     #Pmus finish time (s)

cycles_repetition = [1]                     #repeats n-times the parameter combination

#Fs = [100];           %sample frequency, Hertz (Hz)
#Noise_var = [0 5e-5 5e-6 ];%0.05]; %noise variance over pressure/flow waveforms
#
#Rvent = [1 ];%2];          %ventilator expiratory valve resistance, cmH2O/(L/s)
#C = [30 40 ]%60 80];    %respiratory system compliance, (mL/cmH2O)
#Rins = [8 12 20];     %respiratory system inspiratory resistance, cmH2O/(L/s)
#Rexp = [8 12 20];     %respiratory system  expiratory resistance, cmH2O/(L/s)
#
#PEEP = [5];      %positive end-expiratory pressure, water-centimeters (cmH2O)
#SP = [10];         %support pressure (above PEEP) (cmH2O)
#Triggertype = {'flow' 'pressure' 'delay'};      %ventilator trigger type
#Triggerflow = [2];                          %airflow, (L/min)
#Triggerpressure = [-0.5 -1 -2];                 %pressure (cmH2O)
#Triggerdelay = [0.05 0.10];                     %delay time (s)
#Triggeroption = Triggertype(1);
#Triggerarg = Triggerflow;
#Cycleoff = [0.01 0.25 0.40];                     %turns off the support, after flow fall below x% of the peak flow
#Risetype = {'exp' 'linear'};                    %pressure waveform rises in exponential or linear fashions
#Risetime = [0.3 0.15];                    %time (s) to pressure waveform rises from PEEP to SP
#
#Pmustype = {'linear' 'ingmar' 'parexp'};% 'passive'};        %morphology of the respiratory effort
#Pp = [-5 -10 -15];    %Pmus negative peak amplitude (cmH2O)
#Tp = [0.3 0.5];   %Pmus negative peak time (s)
#Tf = [0.6 1];     %Pmus finish time (s)
#RR = [15];   %respiratory rate, respirations per minute (rpm)
#
#cycles_repetition = 1;                     %repeats n-times the parameter combination